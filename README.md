# TCDCore 2.0

TCDCore 2.0 is a 1.15.2 implimentation of the TCDCore origionally made for The Official Crafting Dead modpack on 1.6.4.

# What tf does it include

  - As of now its pretty basic. It has two major components finished with those being:
        - A pretty expansive playerbase that includes a file structure that I would consider an upgrade from the previous version.
        - It also has a staff extention of the regular TCDPlayer right now the only addition with that extention is a new staff.yml config (Aside from the regular [score.yml , player.yml , killstreak.yml]) as of now it only has one value: an enum that holds the Staff's rank

