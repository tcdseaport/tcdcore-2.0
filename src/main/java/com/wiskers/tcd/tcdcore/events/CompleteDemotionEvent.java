package com.wiskers.tcd.tcdcore.events;

import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CompleteDemotionEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final TCDStaff staffMember;

    public CompleteDemotionEvent(TCDStaff staffMember) {
        this.staffMember = staffMember;
    }

    public TCDStaff getStaffMember() {
        return staffMember;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}