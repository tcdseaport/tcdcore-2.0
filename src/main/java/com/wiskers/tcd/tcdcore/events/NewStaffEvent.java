package com.wiskers.tcd.tcdcore.events;

import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NewStaffEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final TCDStaff staffMember;
    private final Player player;

    public NewStaffEvent(TCDStaff staffMember) {
        this.staffMember = staffMember;
        this.player = staffMember.getTcdPlayer().getPlayer();
    }

    public TCDStaff getStaffMember() {
        return staffMember;
    }

    public Player getPlayer() {
        return player;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}