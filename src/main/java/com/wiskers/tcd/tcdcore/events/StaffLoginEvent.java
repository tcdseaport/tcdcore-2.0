package com.wiskers.tcd.tcdcore.events;

import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;

public class StaffLoginEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final TCDStaff staffMember;
    private final PlayerJoinEvent pjEvent;

    public StaffLoginEvent(TCDStaff staffMember, PlayerJoinEvent event) {
        this.staffMember = staffMember;
        this.pjEvent = event;
    }

    public PlayerJoinEvent getPlayerJoinEvent() {
        return pjEvent;
    }

    public TCDStaff getStaffMember() {
        return staffMember;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}
