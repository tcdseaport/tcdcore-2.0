package com.wiskers.tcd.tcdcore.events;

import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;

public class UserLoginEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final TCDPlayer player;
    private final PlayerJoinEvent pjEvent;

    public UserLoginEvent(TCDPlayer player, PlayerJoinEvent event) {
        this.player = player;
        this.pjEvent = event;
    }

    public PlayerJoinEvent getPlayerJoinEvent() {
        return pjEvent;
    }

    public TCDPlayer getPlayer() {
        return player;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
}

