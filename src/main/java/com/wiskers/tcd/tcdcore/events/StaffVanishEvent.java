package com.wiskers.tcd.tcdcore.events;

import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class StaffVanishEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final TCDStaff staff;
    private final boolean vanished;

    public StaffVanishEvent(TCDStaff staff, boolean vanish) {
        this.staff = staff;
        this.vanished = vanish;
    }

    public TCDStaff getStaff() {
        return staff;
    }

    public boolean isVanished() {
        return vanished;
    }

    @Override
    public @NotNull HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
