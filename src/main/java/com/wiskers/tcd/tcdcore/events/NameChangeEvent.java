package com.wiskers.tcd.tcdcore.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class NameChangeEvent extends Event {
    private static final HandlerList handlers = new HandlerList();

    private final String oldName, newName;

    public NameChangeEvent(String oName, String nName) {
        this.oldName = oName;
        this.newName = nName;
    }

    public String getNewName() {
        return newName;
    }

    public String getOldName() {
        return oldName;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
