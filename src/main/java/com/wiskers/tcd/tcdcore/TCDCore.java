package com.wiskers.tcd.tcdcore;

import com.wiskers.tcd.tcdcore.command.commands.home.DelHomeCommand;
import com.wiskers.tcd.tcdcore.command.commands.home.HomeCommand;
import com.wiskers.tcd.tcdcore.command.commands.home.SetHomeCommand;
import com.wiskers.tcd.tcdcore.command.commands.staff.StaffListCommand;
import com.wiskers.tcd.tcdcore.command.commands.staff.StfManAddCommand;
import com.wiskers.tcd.tcdcore.command.commands.staff.StfManDelCommand;
import com.wiskers.tcd.tcdcore.listeners.UserListener;
import com.wiskers.tcd.tcdcore.user.UserManager;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class TCDCore extends JavaPlugin {

    private boolean debug = true;

    //Instance
    private static TCDCore instance;

    //Manager Instances
    private UserManager userManager;

    @Override
    public void onEnable() {
        setInstance(this);
        registerManagers();
        registerListeners();
        registerCommands();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void registerManagers() {
        this.userManager = new UserManager();
    }

    private void registerListeners() {
        loadListener(userManager, this);
        loadListener(new UserListener(this), this);
    }

    private void registerCommands() {
        loadTabCommand("sethome", new SetHomeCommand());
        loadTabCommand("delhome", new DelHomeCommand());
        loadTabCommand("home", new HomeCommand());

        loadTabCommand("stfmanadd", new StfManAddCommand());
        loadTabCommand("stfmandel", new StfManDelCommand());
        loadTabCommand("stafflist", new StaffListCommand());
    }

    private void loadListener(Listener lis, Plugin plugin) {
        this.getServer().getPluginManager().registerEvents(lis, plugin);
    }

    private void loadCommand(String cName, CommandExecutor cExecutor) {
        getCommand(cName).setExecutor(cExecutor);
    }
    private void loadTabCommand(String cName, CommandExecutor cExecutor) {
        getCommand(cName).setTabCompleter((TabCompleter)cExecutor);
        getCommand(cName).setExecutor(cExecutor);
    }

    private static void setInstance(TCDCore tcdCore) {
        instance = tcdCore;
    }

    public static TCDCore getCurrentInstance() {
        return instance;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public boolean isDevelopment() {
        return debug;
    }
}
