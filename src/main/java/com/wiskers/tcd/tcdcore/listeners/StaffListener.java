package com.wiskers.tcd.tcdcore.listeners;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.events.StaffVanishEvent;
import com.wiskers.tcd.tcdcore.events.UserLoginEvent;
import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerAttemptPickupItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class StaffListener implements Listener {
    //TODO: ~~~~ Vanish ~~~~
    @EventHandler
    public void onStaffVanish(StaffVanishEvent vanishEvent) {
        if(vanishEvent.isVanished()) {
            for(Player player : Bukkit.getOnlinePlayers()) {
                player.hidePlayer(TCDCore.getCurrentInstance(), vanishEvent.getStaff().getTcdPlayer().getPlayer());
            }
            vanishEvent.getStaff().getTcdPlayer().sendMessageWM("You have been vanished.");
            return;
        }
        for(Player player : Bukkit.getOnlinePlayers()) {
            player.showPlayer(TCDCore.getCurrentInstance(), vanishEvent.getStaff().getTcdPlayer().getPlayer());
            vanishEvent.getStaff().getTcdPlayer().sendMessageWM("You are now visible.");
        }
    }

    public void onUserJoin(UserLoginEvent event) {
        for(TCDStaff staff : TCDCore.getCurrentInstance().getUserManager().getStaffMembers()) {
            if(staff.isVanished()) {
                event.getPlayer().getPlayer().hidePlayer(TCDCore.getCurrentInstance(), staff.getTcdPlayer().getPlayer());
            }
        }
    }

    public void onPlayerLeave(PlayerQuitEvent event) {
        if(!TCDCore.getCurrentInstance().getUserManager().isStaff(event.getPlayer())) {
            for(TCDStaff staff : TCDCore.getCurrentInstance().getUserManager().getStaffMembers()) {
                if (staff.isVanished()) {
                    event.getPlayer().showPlayer(event.getPlayer());
                }
            }
        }
    }
    @EventHandler
    public void onEntityTarget(EntityTargetEvent event) {
        if(event.getTarget() instanceof Player && TCDCore.getCurrentInstance().getUserManager().isStaff((Player)event.getTarget())) {
            TCDStaff staff = TCDCore.getCurrentInstance().getUserManager().getStaffMember((Player)event.getTarget());
            if(staff.isVanished()) {
                event.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void onDamageByBlock(EntityDamageByBlockEvent event) {
        if(event.getEntity() instanceof Player && TCDCore.getCurrentInstance().getUserManager().isStaff((Player)event.getEntity())) {
            TCDStaff staff = TCDCore.getCurrentInstance().getUserManager().getStaffMember((Player)event.getEntity());
            if(staff.isVanished()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onDamageByEntity(EntityDamageByEntityEvent event) {
        if(event.getDamager() instanceof Player && TCDCore.getCurrentInstance().getUserManager().isStaff((Player)event.getDamager())) {
            TCDStaff staff = TCDCore.getCurrentInstance().getUserManager().getStaffMember((Player)event.getDamager());
            if(staff.isVanished()) {
                event.setCancelled(true);
            }
        } else if(event.getEntity() instanceof Player && TCDCore.getCurrentInstance().getUserManager().isStaff((Player)event.getEntity())) {
            TCDStaff staff = TCDCore.getCurrentInstance().getUserManager().getStaffMember((Player)event.getEntity());
            if(staff.isVanished()) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onItemPickup(PlayerAttemptPickupItemEvent event) {
        if(TCDCore.getCurrentInstance().getUserManager().isStaff(event.getPlayer())) {
            TCDStaff staff = TCDCore.getCurrentInstance().getUserManager().getStaffMember(event.getPlayer());
            if(staff.isVanished()) {
                event.setCancelled(true);
            }
        }
    }
}
