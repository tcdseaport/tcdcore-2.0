package com.wiskers.tcd.tcdcore.listeners;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.events.*;
import com.wiskers.tcd.tcdcore.user.UserManager;
import com.wiskers.tcd.tcdcore.utils.TCDStrings;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class UserListener implements Listener {
    private TCDCore core;

    public UserListener(TCDCore core) {
        this.core = core;
    }

    @EventHandler
    public void onCompleteDemotion(CompleteDemotionEvent event) {
        try {
            File archive = new File(event.getStaffMember().getTcdPlayer().getTcdPlayerDir(), "archive");
            if(!archive.exists()) archive.mkdirs();
            Files.move(event.getStaffMember().getStaffFile().toPath(), new File(archive, "staff.yml").toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onNameChange(NameChangeEvent event) {

    }

    @EventHandler
    public void onStaffCreation(NewStaffEvent event) {

    }

    @EventHandler
    public void onStaffLogin(StaffLoginEvent event) {
        event.getPlayerJoinEvent().setJoinMessage(null);
        event.getStaffMember().getTcdPlayer().sendMessage(TCDStrings.staffWelcome(event.getPlayerJoinEvent().getPlayer()));
    }

    @EventHandler
    public void onUserLogin(UserLoginEvent event) {
        event.getPlayerJoinEvent().setJoinMessage(null);
        event.getPlayer().sendMessage(TCDStrings.playerWelcome(event.getPlayerJoinEvent().getPlayer()));
    }
}
