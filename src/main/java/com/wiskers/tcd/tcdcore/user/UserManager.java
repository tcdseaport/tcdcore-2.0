package com.wiskers.tcd.tcdcore.user;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.events.CompleteDemotionEvent;
import com.wiskers.tcd.tcdcore.events.NewStaffEvent;
import com.wiskers.tcd.tcdcore.events.StaffLoginEvent;
import com.wiskers.tcd.tcdcore.events.UserLoginEvent;
import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.objects.staff.DefaultStaffRanks;
import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserManager implements Listener {
    private final TCDCore core = TCDCore.getCurrentInstance();

    private final File pluginDir = core.getDataFolder();
    private final File playersDir = new File(pluginDir, "players");
    private final File staffDir = new File(pluginDir, "staff");

    //TODO: CFG files

    //TODO: FileConfigurations
    private FileConfiguration cStaffRanks;

        //TODO: config defaults
    private final List<String> dEnumStaffRanks = Stream.of(DefaultStaffRanks.values()).map(DefaultStaffRanks::name).collect(Collectors.toList());
    private final List<String> dPrettyStaffRanks = Stream.of(DefaultStaffRanks.values()).map(DefaultStaffRanks::getName).collect(Collectors.toList());

    private final List<TCDPlayer> players = new ArrayList<>();
    private final List<TCDStaff> staffMembers = new ArrayList<>();

    public UserManager() {
        if(!staffDir.exists()) {
            staffDir.mkdirs();
        }
    }

    public List<TCDPlayer> getPlayers() {
        return players;
    }

    public File getPlayerDir(TCDPlayer player) {
        return player.getTcdPlayerDir();
    }

    public File getPlayerDir(UUID uuid) {
        return getPlayerFromUUID(uuid).getTcdPlayerDir();
    }

    public TCDPlayer getPlayerFromUUID(UUID uuid) {
        for(TCDPlayer player : getPlayers()) {
            if(player.getUUID() == uuid) return player;
        }
        return null;
    }

    public TCDPlayer getPlayerFromBukkitPlayer(Player player) {
        for(TCDPlayer tcdPlayer : getPlayers()) {
            if(tcdPlayer.getPlayer() == player) return tcdPlayer;
        }
        return null;
    }

    public TCDPlayer getPlayerFromCurrentUsername(String username) {
        for(TCDPlayer player : getPlayers()) {
            if(player.getPlayer().getName().equals(username)) return player;
        }
        return null;
    }

    public void initializePlayer(Player player) {
        TCDPlayer tcdPlayer = new TCDPlayer(player.getUniqueId());
        getPlayers().add(tcdPlayer);
    }

    public void initializeStaffMember(TCDPlayer player) {
        TCDStaff staff = new TCDStaff(player, "MODERATOR");
        getStaffMembers().add(staff);
    }

    public void setStaff(TCDPlayer player) {
        if(new File(getPlayerDir(player), "staff.yml").exists()) {
            return;
        }
        TCDStaff staff = new TCDStaff(player, "MODERATOR");
        Bukkit.getPluginManager().callEvent(new NewStaffEvent(staff));
        getStaffMembers().add(staff);
    }

    public void setStaff(TCDPlayer player, String rank) {
        TCDStaff staff = new TCDStaff(player, rank);
        Bukkit.getPluginManager().callEvent(new NewStaffEvent(staff));
        getStaffMembers().add(staff);
    }

    public void setStaff(TCDPlayer player, DefaultStaffRanks rank) {
        if(new File(getPlayerDir(player), "staff.yml").exists()) {
            return;
        }
        TCDStaff staff = new TCDStaff(player, rank.getName());
        Bukkit.getPluginManager().callEvent(new NewStaffEvent(staff));
        getStaffMembers().add(staff);
    }


    public void removeStaff(TCDStaff staff) {
        if(staff.getTcdPlayer().isStaff()) {
            staff.getTcdPlayer().setStaff(false);
            Bukkit.getPluginManager().callEvent(new CompleteDemotionEvent(staff));
            this.staffMembers.remove(staff);
        }
    }

    public void deconstructPlayer(Player player) {
        if(staffMembers.contains(getStaffMember(player))) {
            staffMembers.remove(getStaffMember(player));
        }
        if(getPlayerFromUUID(player.getUniqueId()) != null) {
            getPlayers().remove(getPlayerFromUUID(player.getUniqueId()));
        }
    }


    /**
     * @return if the player has
     */
    public boolean isStaff(TCDPlayer player) {
        return player.isStaff();
    }

    public boolean isStaff(Player player) {
        TCDPlayer tcdPlayer = new TCDPlayer(player.getUniqueId());
        return tcdPlayer.isStaff();
    }

    public TCDCore getCore() {
        return core;
    }

    public File getPlayersDir() {
        return playersDir;
    }

    public List<TCDStaff> getStaffMembers() {
        return staffMembers;
    }

    /**
     * @return all online staff in one formatted string
     */
    public String onlineStaffString() {
        StringBuilder string = new StringBuilder();
        for(TCDStaff staff : staffMembers) {
            string.append(staff.getTcdPlayer().getPlayer().getDisplayName() + ", ");
        }
        String staffList = string.toString();
        if(staffList.endsWith(",")) staffList = staffList.substring(0, staffList.length() - 1) + ".";
        return staffList;
    }

    /**
     * @return a list of the usernames of all online staff.
     */
    public List<String> onlineStaff() {
        List<String> staffmembers = new ArrayList<>();
        for(TCDStaff staff : staffMembers) {
            staffmembers.add(staff.getTcdPlayer().getCurrentUsername());
        }
        return staffmembers;
    }

    /**
     * @return returns the amount of staff members currently on the server
     */
    public int onlineStaffLength() {
        return staffMembers.toArray().length;
    }

    /**
     * @return all online users in one formatted string
     */
    public String onlineUsersString() {
        StringBuilder string = new StringBuilder();
        for(TCDPlayer player : this.players) {
            string.append(player.getPlayer().getDisplayName() + ", ");
        }
        String userList = string.toString();
        if(userList.endsWith(",")) userList = userList.substring(0, userList.length() - 1) + ".";
        return userList;
    }

    /**
     * @return a list of the usernames of all online users.
     */
    public List<String> onlineUsers() {
        List<String> users = new ArrayList<>();
        for(TCDPlayer user : players) {
            users.add(user.getCurrentUsername());
        }
        return users;
    }

    /**
     * @return the amount of online users
     */
    public int onlineUsersLength() {
        return players.toArray().length;
    }


    public List<String> getLowerCaseStaffRanks() {
        return dPrettyStaffRanks;
    }

    public TCDStaff getStaffMember(Player player) {
        for(TCDStaff staff : staffMembers) {
            if(staff.getTcdPlayer().getPlayer() == player) {
                return staff;
            }
        }
        return null;
    }

    public TCDStaff getStaffMember(UUID uuid) {
        for(TCDStaff staff : staffMembers) {
            if(staff.getTcdPlayer().getPlayer().getUniqueId() == uuid) {
                return staff;
            }
        }
        return null;
    }

    @Deprecated
    public TCDStaff getStaffMember(String username) {
        for(TCDStaff staff : staffMembers) {
            if(staff.getTcdPlayer().getPlayer().getName() == username) {
                return staff;
            }
        }
        return null;
    }


    //TODO: ~~~~ UserManager Event Listeners ~~~~

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        TCDPlayer ePlayer = new TCDPlayer(event.getPlayer().getUniqueId());
        core.getUserManager().initializePlayer(event.getPlayer());
        if(ePlayer.isStaff()) {
            core.getUserManager().initializeStaffMember(ePlayer);
            Bukkit.getPluginManager().callEvent(new StaffLoginEvent(core.getUserManager().getStaffMember(event.getPlayer()), event));
        } else {
            core.getUserManager().initializePlayer(event.getPlayer());
            Bukkit.getPluginManager().callEvent(new UserLoginEvent(new TCDPlayer(event.getPlayer().getUniqueId()), event));
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        core.getUserManager().deconstructPlayer(event.getPlayer());
    }
}
