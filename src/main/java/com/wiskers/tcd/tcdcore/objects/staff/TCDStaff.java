package com.wiskers.tcd.tcdcore.objects.staff;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.events.NewStaffEvent;
import com.wiskers.tcd.tcdcore.events.StaffVanishEvent;
import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

public class TCDStaff {

    private boolean vanish, god, spectator;

    private final TCDPlayer tcdPlayer;
    private DefaultStaffRanks staffRank;
    private final File fStaff;
    private FileConfiguration cStaff;

    public TCDStaff(TCDPlayer tcdPlayer, String rank) {
        this.tcdPlayer = tcdPlayer;

        fStaff = new File(tcdPlayer.getTcdPlayerDir(), "staff.yml");
        if(!fStaff.exists()) this.newStaffMember(tcdPlayer);
        if(!tcdPlayer.isStaff()) {
            tcdPlayer.setStaff(true);
        }

        tcdPlayer.setStaff(true);
        if(cStaff == null) this.cStaff = YamlConfiguration.loadConfiguration(fStaff);
        this.tcdPlayer.addConfigFile(fStaff);
        this.tcdPlayer.addConfig(cStaff);

        if(!cStaff.contains("rank")) {
            cStaff.set("rank", rank);
            cStaff.set("vanish", false);
            cStaff.set("god", false);
            cStaff.set("spectator", false);
            saveStaffConfig();
        }
        this.loadValues();
    }

    private void newStaffMember(TCDPlayer tcdPlayer) {
        try {
            fStaff.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bukkit.getPluginManager().callEvent(new NewStaffEvent(this));
    }

    private void loadValues() {
        this.staffRank = DefaultStaffRanks.getRankFromName(cStaff.getString("rank"));
    }

    public void promote2Owner() {
        this.staffRank = DefaultStaffRanks.OWNER;
        cStaff.set("rank", staffRank.getName());
        saveStaffConfig();
        this.tcdPlayer.saveAll();
    }

    public void promote2Manager() {
        this.staffRank = DefaultStaffRanks.MANAGER;
        cStaff.set("rank", staffRank.getName());
        saveStaffConfig();
        this.tcdPlayer.saveAll();
    }

    public void promote2Dev() {
        this.staffRank = DefaultStaffRanks.DEVELOPER;
        cStaff.set("rank", staffRank.getName());
        saveStaffConfig();
        this.tcdPlayer.saveAll();
    }

    public void promote2HeadAdmin() {
        this.staffRank = DefaultStaffRanks.HEADADMIN;
        cStaff.set("rank", staffRank.getName());
        saveStaffConfig();
        this.tcdPlayer.saveAll();
    }

    public void promote2Admin() {
        this.staffRank = DefaultStaffRanks.ADMIN;
        cStaff.set("rank", staffRank.getName());
        saveStaffConfig();
        this.tcdPlayer.saveAll();
    }

    public void demote2Mod() {
        this.staffRank = DefaultStaffRanks.MODERATOR;
        cStaff.set("rank", staffRank.getName());
        saveStaffConfig();
        this.tcdPlayer.saveAll();
    }

    public void saveStaffConfig() {
        try {
            this.cStaff.save(fStaff);
        } catch (IOException e) {
            TCDCore.getCurrentInstance().getLogger().log(Level.SEVERE, "Failed to save staff.yml");
        }
    }

    @Deprecated
    public void setGod(boolean god) {
        this.cStaff.set("god", god);
        this.saveStaffConfig();
        this.god = god;
    }

    public boolean isGod() {
        if(!cStaff.contains("god")) {
            setGod(false);
        }
        return cStaff.getBoolean("god");
    }
    
    public void toggleGod() {
        this.setGod(!isGod());
    }

    public void setVanish(boolean vanish) {
        Bukkit.getPluginManager().callEvent(new StaffVanishEvent(this, vanish));
        this.cStaff.set("vanish", vanish);
        this.saveStaffConfig();
        this.vanish = vanish;
    }

    public boolean isVanished() {
        if(!cStaff.contains("vanish")) {
            setVanish(false);
        }
        return cStaff.getBoolean("vanish");
    }

    public void toggleVanish() {
        this.setVanish(!isVanished());
    }

    public void setSpectator(boolean spectator) {
        this.cStaff.set("spectator", spectator);
        this.saveStaffConfig();
        this.spectator = spectator;
    }

    public boolean isSpectator() {
        if(!cStaff.contains("spectator")) {
            setSpectator(false);
        }
        return cStaff.getBoolean("spectator");
    }

    public void toggleSpectator() {
        this.spectator = !spectator;
    }

    public FileConfiguration getStaffConfiguration() {
        return cStaff;
    }

    public File getStaffFile() {
        return fStaff;
    }

    public TCDPlayer getTcdPlayer() {
        return tcdPlayer;
    }
}
