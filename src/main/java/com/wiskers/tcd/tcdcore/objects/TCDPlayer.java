package com.wiskers.tcd.tcdcore.objects;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.events.NameChangeEvent;
import com.wiskers.tcd.tcdcore.utils.TCDStrings;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class TCDPlayer {
    protected final TCDCore core = TCDCore.getCurrentInstance();

    protected final Player player;
    protected final UUID uuid;
    private boolean staff;

    private List<String> previousUsernames;
    private String currentUsername;

    //TODO: playtime
    //private long playtime;

    //TODO: Homes
    private int maxHomes;
    private List<TCDHome> playerHomes = new ArrayList<TCDHome>();
    private List<String> homeNames = new ArrayList<String>();

    protected int score, kills, deaths;

    protected List<String> currentKillstreak = new ArrayList<String>();
    protected int killstreakNum;

    protected final File pStorageDir = new File(core.getDataFolder(), "players");
    protected final File tcdPlayerDir;

    //CFG files;
    protected File fScore, fPlayer, fKillstreak, fHomes;
    protected List<File> pConfigFiles = new ArrayList<File>();
    //CFG
    protected FileConfiguration cScore, cPlayer,cKillstreak, cHomes;
    protected List<FileConfiguration> pConfigs = new ArrayList<FileConfiguration>();

    public TCDPlayer(UUID uuid) {
        this.uuid = uuid;
        this.player = Bukkit.getPlayer(uuid);
        tcdPlayerDir = new File(pStorageDir, this.uuid.toString());

        this.fScore = new File(tcdPlayerDir, "score.yml");
        this.fPlayer = new File(tcdPlayerDir, "player.yml");
        this.fKillstreak = new File(tcdPlayerDir, "killstreak.yml");
        this.fHomes = new File(tcdPlayerDir, "homes.yml");

        if(!pStorageDir.exists() || !fPlayer.exists()) this.newPlayer(player);

        this.pConfigFiles = new ArrayList<File>();
        pConfigFiles.add(fScore);
        pConfigFiles.add(fPlayer);
        pConfigFiles.add(fKillstreak);
        pConfigFiles.add(fHomes);

        if(cScore == null) this.cScore = YamlConfiguration.loadConfiguration(fScore);
        if(cPlayer == null) this.cPlayer = YamlConfiguration.loadConfiguration(fPlayer);
        if(cKillstreak == null) this.cKillstreak = YamlConfiguration.loadConfiguration(fKillstreak);
        if(cHomes == null) this.cHomes = YamlConfiguration.loadConfiguration(fHomes);

        this.pConfigs = new ArrayList<FileConfiguration>();
        pConfigs.add(cScore);
        pConfigs.add(cPlayer);
        pConfigs.add(cKillstreak);
        pConfigs.add(cHomes);

        //TODO: Handle namechange
        if(player.getName() != cPlayer.getString("currentusername")) Bukkit.getServer().getPluginManager().callEvent(new NameChangeEvent(cPlayer.getString("currentusername"), player.getName())); this.addPreviousUsernames(cPlayer.getString("currentusername")); this.setCurrentUsername(player.getName());

        loadValues();
    }

    protected void newPlayer(Player player) {
        try {
            if(!tcdPlayerDir.exists()) tcdPlayerDir.mkdirs();
            if(!fScore.exists()) {
                fScore.createNewFile();
                this.cScore = YamlConfiguration.loadConfiguration(fHomes);
            }
            if(!fPlayer.exists()) {
                fPlayer.createNewFile();
                this.cPlayer = YamlConfiguration.loadConfiguration(fPlayer);
            }
            if(!fKillstreak.exists()) {
                fKillstreak.createNewFile();
                this.cKillstreak = YamlConfiguration.loadConfiguration(fKillstreak);
            }
            if(!fHomes.exists()) {
                fHomes.createNewFile();
                this.cHomes = YamlConfiguration.loadConfiguration(fHomes);
            }
            this.setDefaults();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setDefaults() throws IOException {
        this.cScore.set("score", 0);
        this.cScore.set("kills", 0);
        this.cScore.set("deaths", 0);
        this.cScore.save(fScore);

        //TODO: Load Player
        this.cPlayer.set("uuid", this.uuid.toString());
        this.cPlayer.set("currentusername", player.getName());
        this.cPlayer.set("usernames", previousUsernames);
        this.cPlayer.set("staff", false);
        this.cPlayer.save(fPlayer);

        //TODO: Load killstreaks
        this.cKillstreak.set("killstreaknum", 0);
        this.cKillstreak.set("killstreaklist", new ArrayList<String>());
        this.cKillstreak.save(fKillstreak);

        //Load homes
        this.cHomes.set("homeNames", new ArrayList<String>());
        this.cHomes.save(fHomes);

    }

    public void loadValues() {
        for(FileConfiguration config : pConfigs) {
            if(config.contains("score")) this.score = config.getInt("score"); this.kills = config.getInt("kills"); this.deaths = config.getInt("deaths");
            if(config.contains("uuid") && config.getString("uuid") == player.getUniqueId().toString()); this.staff = config.getBoolean("staff"); this.previousUsernames = config.getStringList("usernames");
            if(config.contains("killstreaknum")) this.killstreakNum = config.getInt("killstreaknum"); this.currentKillstreak = config.getStringList("killstreaklist");
            if(config.contains("homes")) {
                this.homeNames = config.getStringList("homes");
                for(String home : config.getKeys(false)) {
                    if(home != "x" && home != "y" && home != "z" && home != "homes" && home != "homeNames" && home != "homes") {
                        addHomeName(home);
                    }
                }
                for(String homeName : homeNames) {
                    if(config.contains(homeName + ".x")) {
                        playerHomes.add(new TCDHome(homeName, config.getDouble(homeName + ".x"), config.getDouble(homeName + ".y"), config.getDouble(homeName + ".z")));
                    }
                }
            }
        }
    }

    public void sendMessageWM(String message) {
        player.sendMessage(TCDStrings.tcdWM + message);
    }

    public void sendMessage(String message) {
        player.sendMessage(TCDStrings.tcdColor + message);
    }

    //TODO: Getters and Setters for score

    public void addScore(int addition) {
        this.score = score + addition;
        this.cScore.set("score", cScore.getInt("score") + addition);
        try {
            this.cScore.save(fScore);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save score.yml. Maybe it doesn't exist or is being used by another program.");
        }
    }

    public void setScore(int score) {
        this.score = score;
        this.cScore.set("score", score);
        try {
            this.cScore.save(fScore);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save score.yml. Maybe it doesn't exist or is being used by another program.");
        }
    }

    public void resetScore() {
        this.score = 0;
        setScore(0);
    }

    public int getScore() {
        if(cScore.getInt("score") == this.score) return score;
        return cScore.getInt("score");
    }

    public void setKills(int kills) {
        this.kills = kills;
        this.cScore.set("kills", kills);
        saveScoreConfig();
    }

    public void resetKills() {
        setKills(0);
    }

    public int getKills() {
        if(cScore.getInt("kills") == this.kills) return kills;
        return cScore.getInt("kills");
    }

    public void addDeath() {
        this.deaths = this.deaths++;
        this.cScore.set("deaths", this.cScore.getInt("deaths") + 1);
        saveScoreConfig();
        this.resetCurrentKillstreak();
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
        this.cScore.set("deaths", deaths);
        saveScoreConfig();
    }

    public void resetDeaths() {
        setDeaths(0);
    }

    public int getDeaths() {
        if(cScore.getInt("deaths") == this.deaths) return deaths;
        return cScore.getInt("deaths");
    }

    //TODO: Player Getters
    @Deprecated
    public void setCurrentUsername(String username) {
        this.currentUsername = username;
        this.cPlayer.set("currentusername", username);
        savePlayerConfig();
    }

    //TODO: Getters and setters for killstreaks;


    public void setCurrentKillstreak(List<String> currentKillstreak) {
        this.currentKillstreak = currentKillstreak;
        this.cKillstreak.set("killstreaklist", currentKillstreak);
        saveKillstreakConfig();
    }

    public void addKillstreakNum() {
        setKillstreakNum(killstreakNum++);
    }

    public void setKillstreakNum(int num) {
        this.killstreakNum = num;
        this.cKillstreak.set("killstreaknum", num);
        saveKillstreakConfig();
    }

    public void resetKillstreakNum() {
        setKillstreakNum(0);
    }

    public int getKillstreakNum() {
        if(cKillstreak.getInt("killstreaknum") == this.killstreakNum) return killstreakNum;
        return cKillstreak.getInt("killstreaknum");
    }


    //TODO: Kill list to UUID instead of usernames
    public void addKill(String username) {
        this.currentKillstreak.add(username);
        setCurrentKillstreak(currentKillstreak);
        setKills(kills++);
        this.addKillstreakNum();
    }

    public void resetCurrentKillstreak() {
        List<String> empt = new ArrayList<String>();
        setCurrentKillstreak(empt);
    }

    public void saveAll() {
        try {
            for(FileConfiguration config : this.pConfigs) {
                config.save(config.getCurrentPath());
            }
            cScore.save(fScore);
            cPlayer.save(fPlayer);
            cKillstreak.save(fKillstreak);
            cHomes.save(fHomes);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save a config.");
        }

    }

    public void saveScoreConfig() {
        try {
            this.cScore.save(fScore);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save score.yml");
        }
    }

    public void savePlayerConfig() {
        try {
            this.cPlayer.save(fPlayer);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save player.yml");
        }

    }

    public void saveHomesConfig() {
        try {
            this.cHomes.save(fHomes);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save homes.yml");
        }
    }

    public void saveKillstreakConfig() {
        try {
            this.cKillstreak.save(fKillstreak);
        } catch (IOException e) {
            core.getLogger().log(Level.SEVERE, "Failed to save killstreak.yml");
        }
    }

    public File getTcdPlayerDir() {
        return tcdPlayerDir;
    }

    public UUID getUUID() {
        return uuid;
    }

    public FileConfiguration getScoreConfiguration() {
        return cScore;
    }

    public FileConfiguration getPlayerConfiguration() {
        return cPlayer;
    }

    public FileConfiguration getKillstreakConfiguration() {
        return cKillstreak;
    }

    public Player getPlayer() {
        return player;
    }

    public List<File> getConfigFiles() {
        return pConfigFiles;
    }

    public List<FileConfiguration> getConfigs() {
        return pConfigs;
    }

    public void addConfiguration(File file, FileConfiguration configuration) {
        this.pConfigFiles.add(file);
        this.pConfigs.add(configuration);
    }

    public TCDCore getCore() {
        return core;
    }

    public List<TCDHome> getPlayerHomes() {
        return playerHomes;
    }

    public void addHomeName(String name) {
        if(name != "homes")
        this.homeNames.add(name);
        if(this.cHomes.getStringList("homes").isEmpty()) {
            this.cHomes.set("homes", new ArrayList<String>(){{add(name);}});
        }
        this.cHomes.set("homes", this.cHomes.getStringList("homes").add(name));
        saveHomesConfig();
    }

    public void removeHomeName(String name) {
        this.homeNames.remove(name);
        this.cHomes.set("homes", this.cHomes.getStringList("homes").remove(name));
        saveHomesConfig();
    }


    public FileConfiguration getHomesConfig() {
        return cHomes;
    }

    public void addHome(TCDHome tcdHome) {
        this.playerHomes.add(tcdHome);
        addHomeName(tcdHome.name);
        this.cHomes.createSection(tcdHome.name);
        this.cHomes.set(tcdHome.name + ".x", tcdHome.x);
        this.cHomes.set(tcdHome.name + ".y", tcdHome.y);
        this.cHomes.set(tcdHome.name + ".z", tcdHome.z);
        this.saveHomesConfig();
    }

    public void removeHome(TCDHome home) {
        playerHomes.remove(home);
        removeHomeName(home.getName());
        this.cHomes.set(home.name, null);
        saveHomesConfig();
    }

    public void removeHome(String homeName) {
        if(cHomes.get(homeName + ".x") == null) {
            return;
        }
        TCDHome home = getHomeFromName(homeName);
        playerHomes.remove(home);
        removeHomeName(home.getName());
        this.cHomes.set(home.name, null);
        saveHomesConfig();
    }

    public TCDHome getHomeFromName(String homeName) {
        if(cHomes.get(homeName + ".x") == null) {
            return null;
        }
        return new TCDHome(homeName, cHomes.getDouble(homeName + ".x"), cHomes.getDouble(homeName + ".y"), cHomes.getDouble(homeName + ".z"));
    }

    public Location getHomeLocation(TCDHome home) {
        return home.getLocation();
    }

    public List<String> getHomeNames() {
        return homeNames;
    }

    public TCDHome newHome(String name, Location loc) {
        return new TCDHome(name, loc);
    }

    public void addNewHome(String name, Location loc) {
        addHome(newHome(name, loc));
    }

    public void addConfigFile(File file) {
        if(!pConfigFiles.contains(file)) {
            pConfigFiles.add(file);
        }
    }
    public void addConfig(FileConfiguration file) {
        if(!pConfigs.contains(file)) {
            pConfigs.add(file);
        }
    }

    public void setStaff(boolean staff) {
        cPlayer.set("staff", staff);
        savePlayerConfig();
        this.staff = staff;
    }

    public boolean isStaff() {
        if(cPlayer.getBoolean("staff") == true) {
            this.staff = true;
            return true;
        }
        this.staff = false;
        return false;
    }

    public String getCurrentUsername() {
        return currentUsername;
    }

    public List<String> getPreviousUsernames() {
        return previousUsernames;
    }

    public void setPreviousUsernames(List<String> previousUsernames) {
        this.cPlayer.set("usernames", previousUsernames);
        savePlayerConfig();
        this.previousUsernames = previousUsernames;
    }

    public void addPreviousUsernames(String username) {
        previousUsernames.add(username);
        this.setPreviousUsernames(previousUsernames);
    }

    //TODO: Home
    public class TCDHome {
        private String name;
        private double x, y, z;

        public TCDHome(String name, double x, double y, double z) {
            this.name = name;
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public TCDHome(String name, Location location) {
            this.name = name;
            this.x = location.getX();
            this.y = location.getY();
            this.z = location.getZ();
        }

        public Location getLocation() {
            return new Location(Bukkit.getWorld("world"), x, y, z);
        }

        public String getName() {
            return name;
        }

        public double getX() {
            return x;
        }

        public double getY() {
            return y;
        }

        public double getZ() {
            return z;
        }

    }
}
