package com.wiskers.tcd.tcdcore.objects.staff;

public enum DefaultStaffRanks {

    OWNER, MANAGER, DEVELOPER, HEADADMIN, ADMIN, MODERATOR;

    private final String name;

    DefaultStaffRanks() {
        name = this.toString().toLowerCase().substring(0, 1).toUpperCase() + this.toString().toLowerCase().substring(1);
    }

    public static DefaultStaffRanks getRankFromName(String name) {
        for(DefaultStaffRanks rank : DefaultStaffRanks.values()) {
            if(rank.getName().equalsIgnoreCase(name)) {
                return rank;
            }
        }
        return MODERATOR;
    }

    public String getName() {
        return name;
    }
}
