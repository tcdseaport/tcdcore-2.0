package com.wiskers.tcd.tcdcore.command.permissions.home;

import org.bukkit.permissions.Permission;

public class SetHomePermission extends Permission {
    public SetHomePermission() {
        super("tcdcore.commands.homes.add");
    }
}