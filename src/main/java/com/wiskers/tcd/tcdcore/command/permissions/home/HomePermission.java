package com.wiskers.tcd.tcdcore.command.permissions.home;

import org.bukkit.permissions.Permission;

public class HomePermission extends Permission {
    public HomePermission() {
        super("tcdcore.commands.homes.teleport");
    }
}
