package com.wiskers.tcd.tcdcore.command.commands.staff;

import com.google.common.collect.Lists;
import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.command.permissions.staff.StfManAddPermission;
import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.objects.staff.TCDStaff;
import com.wiskers.tcd.tcdcore.user.UserManager;
import com.wiskers.tcd.tcdcore.utils.TCDStrings;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class StfManAddCommand implements TabExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if((!sender.hasPermission(new StfManAddPermission()) && !sender.isOp()) || args.length != 2) {
            TCDStrings.noPermission(sender);
            return false;
        }
        switch (args.length){
            case 0:
                //TODO: Gui
                return false;
            case 2:
                TCDPlayer targetPlayer = TCDCore.getCurrentInstance().getUserManager().getPlayerFromBukkitPlayer(Bukkit.getPlayer(args[0]));
                if(targetPlayer != null && TCDCore.getCurrentInstance().getUserManager().getLowerCaseStaffRanks().contains(args[1])) {
                    if(targetPlayer.isStaff()) {
                        sender.sendMessage(args[0] + " is already on the staff team.");
                        return false;
                    }
                    TCDCore.getCurrentInstance().getUserManager().setStaff(targetPlayer, args[1]);
                    targetPlayer.sendMessageWM("You have been given staff. You are now a TCD " + args[1] + ".");
                    targetPlayer.sendMessageWM("Online staff: " + TCDCore.getCurrentInstance().getUserManager().onlineStaff());
                    return true;
                }
                return false;
            default:
                sender.sendMessage("Wrong usage");
                return false;
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if(args.length == 1) {
            List<String> players = new ArrayList<String>();
            for(Player player : Bukkit.getOnlinePlayers()) {
                players.add(player.getName());
            }
            return players;
        }
        switch (args.length) {
            case 1:
                List<String> players = new ArrayList<String>();
                for(Player player : Bukkit.getOnlinePlayers()) {
                    players.add(player.getName());
                }
                return players;
            case 2:
                return new ArrayList<String>() {{addAll(TCDCore.getCurrentInstance().getUserManager().getLowerCaseStaffRanks());}};
            default:
                return Lists.newArrayList();
        }
    }
}
