package com.wiskers.tcd.tcdcore.command.permissions.staff;

import org.bukkit.permissions.Permission;
import org.jetbrains.annotations.NotNull;

public class HomeAdminPermission extends Permission {
    public HomeAdminPermission() {
        super("tcdcore.staff.commands.homeadmin");
    }
}
