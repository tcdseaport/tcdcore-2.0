package com.wiskers.tcd.tcdcore.command.commands.staff;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.command.permissions.staff.StfManAddPermission;
import com.wiskers.tcd.tcdcore.command.permissions.staff.StfManDelPermission;
import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.utils.TCDStrings;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class StfManDelCommand implements TabExecutor {

    Permission permission = new StfManDelPermission();

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if((!sender.isOp()) || !sender.hasPermission(permission)) {
            TCDStrings.noPermission(sender);
            return false;
        }
        switch (args.length){
            case 0:
                //TODO: Gui
                return false;
            case 1:
                TCDPlayer targetPlayer = TCDCore.getCurrentInstance().getUserManager().getPlayerFromBukkitPlayer(Bukkit.getPlayer(args[0]));
                if(!targetPlayer.isStaff()) {
                    sender.sendMessage(args[0] + " is not on the staff team.");
                    return false;
                }
                TCDCore.getCurrentInstance().getUserManager().removeStaff(TCDCore.getCurrentInstance().getUserManager().getStaffMember(Bukkit.getPlayer(args[0])));
                sender.sendMessage(args[0] + " has been removed the staff team.");
                targetPlayer.sendMessageWM("You have been removed from the staff team.");
                return true;

            default:
                sender.sendMessage("Wrong usage");
                return false;
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if(!sender.hasPermission(permission)) {
            return null;
        }
        List<String> staffmembers = new ArrayList<String>();
        for(Player player : Bukkit.getOnlinePlayers()) {
            TCDPlayer tcdPlayer = TCDCore.getCurrentInstance().getUserManager().getPlayerFromBukkitPlayer(player);
            if(tcdPlayer.isStaff()) {
                staffmembers.add(player.getName());
            }
        }
        return staffmembers;
    }
}
