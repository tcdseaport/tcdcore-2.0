package com.wiskers.tcd.tcdcore.command.commands.staff;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.utils.TCDStrings;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class StaffListCommand implements TabExecutor {

    private TCDCore core = TCDCore.getCurrentInstance();

    @Override
    public boolean onCommand(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            commandSender.sendMessage();
            return false;
        }
        TCDPlayer player = core.getUserManager().getPlayerFromBukkitPlayer((Player) commandSender);
        if(!player.isStaff()) {
            player.sendMessageWM("You are not part of the staff team.");
            return false;
        }
        player.sendMessageWM("Online staff: " + TCDCore.getCurrentInstance().getUserManager().onlineStaff());
        return true;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender commandSender, @NotNull Command command, @NotNull String s, @NotNull String[] strings) {
        return new ArrayList<>();
    }
}
