package com.wiskers.tcd.tcdcore.command.permissions.home;


import org.bukkit.permissions.Permission;

public class DelHomePermission extends Permission {
    public DelHomePermission() {
        super("tcdcore.commands.homes.remove");
    }
}
