package com.wiskers.tcd.tcdcore.command.commands.home;

import com.wiskers.tcd.tcdcore.TCDCore;
import com.wiskers.tcd.tcdcore.command.permissions.home.DelHomePermission;
import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import com.wiskers.tcd.tcdcore.utils.TCDStrings;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class DelHomeCommand implements TabExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {

        if(!(sender instanceof Player)) {
            TCDStrings.notPlayer(sender);
            return false;
        }
        if(!(sender.hasPermission(new DelHomePermission())) && !(sender.isOp())) {
            TCDStrings.noPermission(sender);
            return false;
        }
        Player player = (Player) sender;
        TCDPlayer user = TCDCore.getCurrentInstance().getUserManager().getPlayerFromBukkitPlayer(player);
        if(args.length == 1) {
            if(user == null) {
                sender.sendMessage("user is null");
                return false;
            }
            if(!(user.getHomeNames().contains(args[0]))) {
                TCDStrings.invalidHome(user, args[0]);
                return false;
            }
            user.removeHome(args[0]);
            TCDStrings.homeDel(user, args[0]);
            return true;
        }
        return false;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if(!(sender instanceof Player)) {
            TCDStrings.notPlayer(sender);
        }
        if(!(sender.hasPermission(new DelHomePermission())) && !(sender.isOp())) {
            return new ArrayList<String>();
        }
        Player player = (Player) sender;
        TCDPlayer user = TCDCore.getCurrentInstance().getUserManager().getPlayerFromBukkitPlayer(player);
        List<String> homes = new ArrayList<String>();
        for(TCDPlayer.TCDHome home : user.getPlayerHomes()) {
            homes.add(home.getName());
        }
        return homes;
    }
}
