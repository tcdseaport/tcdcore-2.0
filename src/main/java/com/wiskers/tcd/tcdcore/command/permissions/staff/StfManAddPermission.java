package com.wiskers.tcd.tcdcore.command.permissions.staff;

import org.bukkit.permissions.Permission;
import org.jetbrains.annotations.NotNull;

public class StfManAddPermission extends Permission {
    public StfManAddPermission() {
        super("tcdcore.staff.management.stfmanadd");
    }
}
