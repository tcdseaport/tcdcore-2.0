package com.wiskers.tcd.tcdcore.utils;

import com.wiskers.tcd.tcdcore.objects.TCDPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TCDStrings {

    //TODO: Randomization also YML
    public static ChatColor tcdColor = ChatColor.DARK_RED;

    public static String tcdWM = tcdColor + "[TCD] ";

    public static String staffWelcome(Player player) {
        return tcdWM + "Welcome back " + player.getName() + " to TCD. We've missed you alot.";
    }

    public static void teleportToHome(TCDPlayer player, TCDPlayer.TCDHome home) {
        player.sendMessageWM("Teleported to home " + home.getName() + ". Good luck.");
    }

    public static void notPlayer(CommandSender sender) {
        sender.sendMessage(tcdWM + "You have to be ingame to use this command.");
    }

    public static void homeSet(TCDPlayer player, TCDPlayer.TCDHome home) {
        player.sendMessageWM("Home " + home.getName() + " was set to x:" + home.getX() + " y:" + home.getY() + " z:" + home.getZ() + ".");
    }

    public static void homeDel(TCDPlayer player, String home) {
        player.sendMessageWM("Home " + home + " was removed.");
    }

    public static void invalidHome(TCDPlayer player, String home) {
        player.sendMessageWM("Home " + home + " is not a valid home in the database.");
    }

    public static void noPermission(CommandSender sender) {
        sender.sendMessage(tcdWM + "You don't have permission to use this command");
    }

    public static String playerWelcome(Player player) {
        return tcdWM + "Welcome " + player.getName() + " to TCD.";
    }

}
